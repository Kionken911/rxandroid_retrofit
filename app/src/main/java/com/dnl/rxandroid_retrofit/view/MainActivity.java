package com.dnl.rxandroid_retrofit.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dnl.rxandroid_retrofit.R;
import com.dnl.rxandroid_retrofit.presenter.GithubContract;
import com.dnl.rxandroid_retrofit.presenter.GithubPresenter;

public class MainActivity extends AppCompatActivity implements GithubContract.View {

    private GithubPresenter githubPresenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText etUserName = (EditText) findViewById(R.id.etUserName);
        Button btnGetInfo = (Button) findViewById(R.id.btnGetInfo);
        githubPresenter = new GithubPresenter(this, MainActivity.this);

        btnGetInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = etUserName.getText().toString().trim();
                githubPresenter.getProfileGithubUser(userName, "https://api.github.com/");
            }
        });

    }

    @Override
    public void showErrorMessage(String errMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, errMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showProfileInfo(String profileInfo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, profileInfo, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showProgressDialog(String title, String message) {
        dismissProgressDialog();
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
