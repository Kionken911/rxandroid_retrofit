package com.dnl.rxandroid_retrofit.presenter;

import android.app.Activity;
import android.text.TextUtils;

import com.dnl.rxandroid_retrofit.R;
import com.dnl.rxandroid_retrofit.model.Github;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dev.TuanNguyen on 12/2/2017.
 */

public class GithubPresenter implements GithubContract.Presenter {

    private GithubContract.View mView;
    private Retrofit mRetrofit;
    private Activity mActivity;

    public GithubPresenter(GithubContract.View view, Activity activity) {
        this.mView = view;
        this.mActivity = activity;
    }

    @Override
    public void getProfileGithubUser(String userName, String baseUrl) {
        mRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build();

        mView.showProgressDialog("Get info profile", "Loading ...");

        if (!TextUtils.isEmpty(userName)) {
            GithubService githubService = mRetrofit.create(GithubService.class);
            Observable<Github> githubUser = githubService.getGitHubUser(userName);
            githubUser.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorReturn(new Func1<Throwable, Github>() {
                        @Override
                        public Github call(Throwable throwable) {
                            if (throwable != null) {
                                mView.showErrorMessage(throwable.getMessage());
                            }
                            mView.dismissProgressDialog();
                            return null;
                        }
                    })
                    .subscribe(new Action1<Github>() {
                        @Override
                        public void call(Github github) {
                            mView.dismissProgressDialog();
                            if (github != null) {
                                mView.showProfileInfo(github.toString());
                            }
                        }
                    });

        } else {
            mView.dismissProgressDialog();
            mView.showErrorMessage(mActivity.getString(R.string.err_user_name_empty));
        }
    }
}
