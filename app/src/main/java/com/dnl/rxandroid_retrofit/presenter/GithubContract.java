package com.dnl.rxandroid_retrofit.presenter;

/**
 * Created by Dev.TuanNguyen on 12/2/2017.
 */

public interface GithubContract {
    interface Presenter {
        void getProfileGithubUser(String userName,String baseUrl);
    }

    interface View {
        void showErrorMessage(String errMessage);

        void showProfileInfo(String profileInfo);

        void showProgressDialog(String tilte, String message);

        void dismissProgressDialog();
    }
}
