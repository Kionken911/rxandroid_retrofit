package com.dnl.rxandroid_retrofit.presenter;


import com.dnl.rxandroid_retrofit.model.Github;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Dev.TuanNguyen on 12/1/2017.
 */

public interface GithubService {
    @GET("users/{username}")
    Observable<Github> getGitHubUser(@Path("username") String username);
}
